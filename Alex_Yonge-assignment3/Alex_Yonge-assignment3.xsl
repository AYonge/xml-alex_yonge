<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:array="http://www.w3.org/2005/xpath-functions/array" xmlns:map="http://www.w3.org/2005/xpath-functions/map" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:err="http://www.w3.org/2005/xqt-errors" exclude-result-prefixes="array fn map math xhtml xs err" version="3.0">
	<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>

	<!--Author: Alexander Yonge, Class: OOSD Fall 2017-->
	<!--  Great work your, Alex.  Your XSL properly outputs and formats the xml file.  Nicely done!
10/10
-->
	<xsl:template match="/" name="xsl:initial-template">
		<html>
			<head>
				<title>Assignment 3</title>
			</head>
			<body>
				<h1 style="padding-left:6%">Customer Info</h1>
				<p>
					Name:<xsl:value-of select="telephoneBill/customer/name"/><br/>
					Address:<xsl:value-of select="telephoneBill/customer/address"/><br/>
					City:<xsl:value-of select="telephoneBill/customer/city"/><br/>
					Province:<xsl:value-of select="telephoneBill/customer/province"/><br/>
				</p>
				<table border="1" >
					<tbody>
						<tr style="font:bold" align="center">
							<th>Called Number</th>
							<th>Date</th>
							<th>Duration In Minutes</th>
							<th>Charge</th>
						</tr>
						<xsl:for-each select="telephoneBill/call">
							<tr align="center">
								<xsl:if test="fn:position() mod 2=0">
									<xsl:attribute name="bgcolor">hotpink</xsl:attribute>
								</xsl:if>
								<td><xsl:value-of select="@number"/></td>
								<td><xsl:value-of select="@date"/></td>
								<td><xsl:value-of select="@durationInMinutes"/></td>
								<td><xsl:value-of select="@charge"/></td>
							</tr>
						</xsl:for-each>
					</tbody>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
